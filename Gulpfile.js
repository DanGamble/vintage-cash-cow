'use strict';

var gulp = require('gulp');
var browserSync = require('browser-sync');
var babelify = require('babelify');
var browserify = require('browserify');
var source = require('vinyl-source-stream');

var autoPrefixer = require('gulp-autoprefixer');
var babel = require('gulp-babel');
var clean = require('gulp-clean');
var concat = require('gulp-concat');
var pxToRem = require('gulp-pxtorem');
var sass = require('gulp-sass');
var size = require('gulp-size');
var sourceMaps = require('gulp-sourcemaps');
var swig = require('gulp-swig');
var uglify = require('gulp-uglify');
var watch = require('gulp-watch');

gulp.task('serve', function () {
  browserSync({
    server: {
      baseDir: "./dist"
    },
    notify: false,
    open:   false
  });

  watch('./src/assets/scss/**/*.scss', function () {
    gulp.start('styles');
  });

  watch('./src/assets/js/**/*.js', function () {
    gulp.start('es6');
  });

  watch('./src/assets/img/**/*', function () {
    gulp.start('images');
  });

  watch(['./src/**/[^_]*.swig', './src/[^_]*.swig'], function () {
    gulp.start('templates');
  });

});

gulp.task('styles', function () {
  return gulp.src('./src/assets/scss/**/*.scss')
      .pipe(sass({
        errLogToConsole: true,
        precision:       10,
        sourceComments:  true,
        stats:           true
      }))

      .on('error', function (e) {
        console.log(e);
      })

      .pipe(autoPrefixer())
      .pipe(pxToRem())
      .pipe(gulp.dest('./dist/assets/css/'))
      .pipe(browserSync.reload({ stream: true }))
      .pipe(size({ title: 'styles' }));
});

gulp.task('templates', ['clean'], function () {
  gulp.src(['./src/**/[^_]*.swig', './src/[^_]*.swig'])
      .pipe(swig({
        defaults: { cache: false }
      }))

      .on('error', function (error) {
        console.log(error.message);
      })

      .pipe(gulp.dest('./dist'))

      .on('end', function () {
        browserSync.reload();
      });
});

gulp.task('es5ish', function () {
  gulp.src('./src/assets/js/**/*.js')
      .pipe(sourceMaps.init())
      .pipe(babel())
      .pipe(uglify())
      .pipe(concat('all.js'))
      .pipe(sourceMaps.write())
      .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('es6', function () {
  browserify({
    entries: './src/assets/js/main.js',
    debug:   true
  })
      .transform(babelify)
      .bundle()
      .pipe(source('output.js'))
      .pipe(gulp.dest('dist/assets/js'));
});

gulp.task('images', function () {
  gulp.src('./src/assets/img/**/*')
      .pipe(gulp.dest('dist/assets/img'));
});

gulp.task('clean', function () {
  gulp.src('./dist/*.html')
      .pipe(clean());
});

gulp.task('default', ['serve', 'build']);
gulp.task('build', ['templates', 'styles', 'es6', 'images']);
